#pragma once

#include "../oldfit/Types.h"
#include "../fit/Types.h"
#include <cassert>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

// Print mask
#pragma pack (push)
struct quick_and_dirty {
  bool p : 1;
  bool o : 1;
  bool n : 1;
  bool m : 1;
  bool l : 1;
  bool k : 1;
  bool j : 1;
  bool i : 1;
  bool h : 1;
  bool g : 1;
  bool f : 1;
  bool e : 1;
  bool d : 1;
  bool c : 1;
  bool b : 1;
  bool a : 1;
};
#pragma pack (pop)

/**
 * @brief      Instance of the kalman filter
 */
struct Instance {
  bool doForwardFit;
  bool doBackwardFit;
  bool doBismooth;
  bool isOutlier;

  std::vector<std::vector<FitNode>> tracks;
  std::vector<std::vector<FitNode>> expectedResult;
  std::vector<int> outliers;

  // New track type
  std::vector<VectorFit::Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> ntracks;
};

/**
 * @brief      Some useful information about the events
 */
struct GroupInfo {
  int numberOfNodes;
  int filtered, predicted, bismoothed;
  GroupInfo() : numberOfNodes(0), filtered(0), predicted(0), bismoothed(0) {}
};

long double log2 (const long long x);

// Print statements for all
void print (uint16_t b);
void print (const Matrix1x5& m, const std::string& name="Matrix1x5");
void print (const Matrix5x5& m, const std::string& name="Matrix5x5");
void print (const SymMatrix5x5& m, const std::string& name="SymMatrix5x5");
void print (const XYZVector& x, const std::string& name="XYZVector");
void print (const ChiSquare& c, const std::string& name="ChiSquare");
void print (const State& s, const std::string& name="State");
void print (const StateVector& s, const std::string& name="StateVector");
void print (const Node& n, const std::string& name="Node");
void print (const FitNode& f, const std::string& name="FitNode");

bool compare (const Matrix1x5& m1, const Matrix1x5& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const Matrix5x5& m1, const Matrix5x5& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const SymMatrix5x5& m1, const SymMatrix5x5& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const FitNode& f1, const FitNode& f2, const double max_epsilon=1e-03, const bool doPrint=false, const int compareChi2=-1);

void setPrevNode (FitNode& node, FitNode& prevnode, int direction);

std::vector<Instance> translateFileIntoEvent (
  const std::vector<uint8_t>& fileContents,
  VectorFit::GrowingMemManager& memManager,
  const bool checkResults,
  const unsigned minTrackBatchSize
);
bool readBinary (const std::string& filename, std::vector<Instance>& t);
void printInfo (const std::vector<Instance>& instances, std::vector<GroupInfo>& groups);
void postPrint (
  const bool allEqual,
  const bool printInfoAboutGroups,
  const std::vector<Instance>& t,
  const std::vector<GroupInfo>& groups
);

std::string shellexec (const char* cmd);
