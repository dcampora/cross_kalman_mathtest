# Placeholders for machine specific flags
# ADDITIONAL_CXX_FLAGS = -DSP
# ADDITIONAL_LINKFLAGS = -L/opt/intel/tbb/lib/intel64_lin/gcc4.7

ifneq (,$(findstring icc,${CXX}))
	TUNE = -xMIC-AVX512 -no-inline-max-size
else
	CXX = g++
	TUNE = -march=native -fabi-version=6
endif

TARGET = cross_kalman_mathtest
CXXFLAGS = -g -std=c++11 ${TUNE} ${COMPILE_OPTS} ${ADDITIONAL_CXX_FLAGS}
LINKFLAGS = -ltbb -lrt ${ADDITIONAL_LINKFLAGS}
OPTIMIZATION = -O2 -DNDEBUG -DTBB_ON
DEBUG_VARS = -O0 -DDEBUG
SOURCES = main.cpp fit/Types.cpp fit/Scheduler.cpp oldfit/Types.cpp oldfit/Update.cpp oldfit/Predict.cpp oldfit/Similarity.cpp oldfit/MatrixTypes.cpp oldfit/Bismooth.cpp utilities/Tools.cpp
OBJECTS = $(SOURCES:.cpp=.o)

$(TARGET): CXXFLAGS += ${OPTIMIZATION}
$(TARGET): executable

debug: CXXFLAGS += ${DEBUG_VARS}
debug: executable

executable: $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $(TARGET) $(LINKFLAGS)

.cpp.o:
	$(CXX) -c $(CXXFLAGS) -o $@ $<

clean:
	rm -f $(OBJECTS) $(TARGET)

