#pragma once

#include "Aux.h"
#include "fit/AlignedAllocator.h"
#include "fit/Combined.h"
#include "Common.h"

namespace VectorFit {

template<long unsigned W>
struct VectorTest {
  std::mt19937& mt;
  std::uniform_real_distribution<PRECISION>& dist;
  Timer t;
  Timer tshuffle;

  VectorTest (std::mt19937& mt, std::uniform_real_distribution<PRECISION>& dist)
    : mt(mt), dist(dist) {}

  PRECISION fit (
    const unsigned genExperiments,
    const unsigned numExperiments,
    const int printExp = -1
  ) {
    // Generate data
    const unsigned vecGenExperiments = genExperiments / W;
    const unsigned vecExperiments = numExperiments / W;
    
    std::vector<TestNode, aligned_allocator<PRECISION, sizeof(PRECISION)*W>> nodes (genExperiments);
    std::vector<TestNodeOut<W>, aligned_allocator<PRECISION, sizeof(PRECISION)*W>> nodesOut (vecExperiments);

    std::cout << "Generating data" << std::endl;
    for (unsigned i=0; i<vecGenExperiments; ++i) {
      for (unsigned j=0; j<W; ++j) {
        vassign<W>(nodesOut[i].px.data() + j, generate<5>(mt, dist));
        vassign<W>(nodesOut[i].pc.data() + j, generate<15>(mt, dist));
        nodes[i*W + j].Xref = generate<5>(mt, dist);
        nodes[i*W + j].H = generate<5>(mt, dist);
        nodes[i*W + j].refResidual = dist(mt);
        nodes[i*W + j].errorMeas2 = dist(mt);
        nodes[i*W + j].tm = generate<25>(mt, dist);
        nodes[i*W + j].nm = generate<15>(mt, dist);
        nodes[i*W + j].tv = generate<5>(mt, dist);
      }
    }

    // First, shuffle data into an SOA
    std::vector<std::array<PRECISION, 5*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vXref (vecGenExperiments);
    std::vector<std::array<PRECISION, 5*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vH (vecGenExperiments);
    std::vector<std::array<PRECISION, W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vrefResidual (vecGenExperiments);
    std::vector<std::array<PRECISION, W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> verrorMeas2 (vecGenExperiments);
    std::vector<std::array<PRECISION, 25*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vtm (vecGenExperiments);
    std::vector<std::array<PRECISION, 15*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vnm (vecGenExperiments);
    std::vector<std::array<PRECISION, 5*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vtv (vecGenExperiments);

    std::cout << "Preparing data format" << std::endl;
    tshuffle.start();
    for (unsigned i=0; i<vecGenExperiments; ++i) {
      vXref[i] = transpose_helper<5>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 5>& { return nodes[i*W + j].Xref; });
      vH[i] = transpose_helper<5>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 5>& { return nodes[i*W + j].H; });
      vrefResidual[i] = ArrayGen::getRefResidual<W>(nodes, i*W);
      verrorMeas2[i] = ArrayGen::getErrorMeas2<W>(nodes, i*W);
      vtm[i] = transpose_helper<25>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 25>& { return nodes[i*W + j].tm; });
      vnm[i] = transpose_helper<15>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 15>& { return nodes[i*W + j].nm; });
      vtv[i] = transpose_helper<5>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 5>& { return nodes[i*W + j].tv; });
    }
    tshuffle.stop();

    std::cout << "Running experiments" << std::endl;
    t.start();
    for (unsigned i=0; i<vecExperiments; ++i) {
      const unsigned experimentNo = i % vecGenExperiments;

      VectorFit::fit_vec<VectorFit::Op::Forward>::template op<W> (
        // Input is indirected with experimentNo
        vtm[experimentNo],
        vnm[experimentNo],
        vtv[experimentNo],
        vXref[experimentNo],
        vH[experimentNo],
        vrefResidual[experimentNo],
        verrorMeas2[experimentNo],
        nodesOut[experimentNo].px.data(),
        nodesOut[experimentNo].pc.data(),

        // Output is indirected with i
        nodesOut[i].x.data(),
        nodesOut[i].c.data(),
        nodesOut[i].chi2.data()
      );
    }
    t.stop();
    
    PRECISION sum = 0.0;
    for (unsigned i=0; i<vecExperiments; ++i) {
      add(sum, nodesOut[i].x);
      add(sum, nodesOut[i].c);
      add(sum, nodesOut[i].chi2);
    }

    // Print result
    if (printExp >= 0) {
      const unsigned experimentNo = (printExp % genExperiments) / W;
      const unsigned arrayNo = printExp % VECTOR_WIDTH;
      const unsigned elementNo = printExp / VECTOR_WIDTH;

      printState(
        arrayNo,
        nodesOut[experimentNo].px,
        nodesOut[experimentNo].pc,
        nodesOut[elementNo].x,
        nodesOut[elementNo].c,
        nodesOut[elementNo].chi2
      );
    }
    
    return sum;
  }

  PRECISION smoother (
    const unsigned genExperiments,
    const unsigned numExperiments,
    const int printExp = -1
  ) {
    // Generate data
    const unsigned vecGenExperiments = genExperiments / W;
    const unsigned vecExperiments = numExperiments / W;

    std::vector<TestNodeSmoother, aligned_allocator<PRECISION, sizeof(PRECISION)*W>> nodes (genExperiments);
    std::vector<TestNodeOutSmoother<W>, aligned_allocator<PRECISION, sizeof(PRECISION)*W>> nodesOut (vecExperiments);

    std::array<PRECISION, 5> fps {244.325, 676.173, 0.136044, 0.0772327, 0.000112579};
    std::array<PRECISION, 15> fpc {0.102365, -0.799746, 11.3081, -0.000692533, 0.00466304, 5.80635e-06, 0.00101817, -0.015429, -5.50663e-06, 2.38664e-05, 4.18818e-05, -0.000268874, -3.68219e-07, 3.07515e-07, 2.3879e-08};
    std::array<PRECISION, 5> bus {244.329, 674.319, 0.135121, 0.0771319, 0.000141583};
    std::array<PRECISION, 15> buc {0.00886496, 0.0369227, 1.16325, 1.50821e-05, 7.12345e-05, 7.14996e-08, 7.25901e-06, 0.000232638, 1.3873e-08, 1.72189e-07, 9.7893e-09, 4.36583e-08, 1.99743e-11, 3.38434e-12, 1.51093e-13};

    for (unsigned i=0; i<vecGenExperiments; ++i) {
      for (unsigned j=0; j<W; ++j) {
        vassign<W>(nodesOut[i].s1.data() + j, fps);
        vassign<W>(nodesOut[i].c1.data() + j, fpc);
        vassign<W>(nodesOut[i].s2.data() + j, bus);
        vassign<W>(nodesOut[i].c2.data() + j, buc);
        nodes[i*W + j].pm = generate<5>(mt, dist);
        nodes[i*W + j].pa = generate<5>(mt, dist);
        nodes[i*W + j].refResidual = dist(mt);
        nodes[i*W + j].errorMeas2 = dist(mt);
      }
    }

    std::vector<std::array<PRECISION, 5*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vpm (vecGenExperiments);
    std::vector<std::array<PRECISION, 5*W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vpa (vecGenExperiments);
    std::vector<std::array<PRECISION, W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> vrefResidual (vecGenExperiments);
    std::vector<std::array<PRECISION, W>, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> verrorMeas2 (vecGenExperiments);

    tshuffle.start();
    for (unsigned i=0; i<vecGenExperiments; ++i) {
      // First, shuffle data into an SOA
      vpm[i] = transpose_helper<5>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 5>& { return nodes[i*W + j].pm; });
      vpa[i] = transpose_helper<5>::get<W>([&nodes, &i] (int j) -> std::array<PRECISION, 5>& { return nodes[i*W + j].pa; });
      vrefResidual[i] = ArrayGen::getRefResidual<W>(nodes, i*W);
      verrorMeas2[i] = ArrayGen::getErrorMeas2<W>(nodes, i*W);
    }
    tshuffle.stop();

    t.start();
    for (unsigned i=0; i<vecExperiments; ++i) {
      const unsigned experimentNo = i % vecGenExperiments;

      uint16_t r = VectorFit::full_smoother_vec<W> (
        vpm[experimentNo],
        vpa[experimentNo],
        verrorMeas2[experimentNo],
        vrefResidual[experimentNo],
        nodesOut[experimentNo].s1.data(),
        nodesOut[experimentNo].c1.data(),
        nodesOut[experimentNo].s2.data(),
        nodesOut[experimentNo].c2.data(),
        
        nodesOut[i].ss.data(),
        nodesOut[i].sc.data(),
        nodesOut[i].res.data(),
        nodesOut[i].errRes.data()
      );
    }
    t.stop();

    PRECISION sum = 0.0;
    for (unsigned i=0; i<vecExperiments; ++i) {
      add(sum, nodesOut[i].ss);
      add(sum, nodesOut[i].sc);
      add(sum, nodesOut[i].res);
      add(sum, nodesOut[i].errRes);
    }

    // Print result
    if (printExp >= 0) {
      const unsigned experimentNo = (printExp % genExperiments) / W;
      const unsigned arrayNo = printExp % VECTOR_WIDTH;
      const unsigned elementNo = printExp / VECTOR_WIDTH;

      printSmoothState(
        arrayNo,
        nodesOut[experimentNo].s1,
        nodesOut[experimentNo].c1,
        nodesOut[experimentNo].s2,
        nodesOut[experimentNo].c2,
        nodesOut[elementNo].ss,
        nodesOut[elementNo].sc,
        nodesOut[elementNo].res,
        nodesOut[elementNo].errRes
      );
    }

    return sum;
  }
};

}

