Cross architecture Kalman Filter Math Test
==========================================

Welcome to the cross architecture Kalman Filter Math Test project!

This synthethic benchmark is all about testing the math of the Kalman filter in
a simple and efficient way. It can be used as an indicator of the expected
throughput across different architectures.

This is a pet project from the bigger
[Cross Kalman](https://gitlab.cern.ch/dcampora/cross_kalman).

Compiling and running
---------------------

Cross Kalman Math Test lives in four different forms, namely:

* *master*: An implementation in
  [Vectorclass](http://agner.org/optimize/#vectorclass), works well in x86_64
  processors.
  
        git clone https://gitlab.cern.ch/dcampora/cross_kalman_mathtest.git
        cd cross_kalman_mathtest
        mkdir vectorclass && cd vectorclass
        wget http://www.agner.org/optimize/vectorclass.zip
        unzip vectorclass.zip
        cd ..

* *umesimd*: An implementation in [UMESimd](https://github.com/edanor/umesimd).
  Works in x86_64, ARM64 and Power8 processors (for an updated list, check with
  the library's documentation).
  
        git clone https://gitlab.cern.ch/dcampora/cross_kalman_mathtest.git
        cd cross_kalman_mathtest
        git checkout umesimd
        git clone https://github.com/edanor/umesimd.git
  
* *cuda*: Manycore CUDA implementation. It can run on NVIDIA GPUs.

        git clone https://gitlab.cern.ch/dcampora/cross_kalman_mathtest.git
        cd cross_kalman_mathtest
        git checkout cuda
        mkdir vectorclass && cd vectorclass
        wget http://www.agner.org/optimize/vectorclass.zip
        unzip vectorclass.zip
        cd ..
  
* *opencl*: Manycore implementation. It can run on any device supporting OpenCL,
  which includes x86_64, NVIDIA GPUs, AMD GPUs, and certain FPGAs.

        git clone https://gitlab.cern.ch/dcampora/cross_kalman_mathtest.git
        cd cross_kalman_mathtest
        git checkout opencl
        mkdir vectorclass && cd vectorclass
        wget http://www.agner.org/optimize/vectorclass.zip
        unzip vectorclass.zip
        cd ..

Compiling and running,

    make
    ./cross_kalman_mathtest

In the case of OpenCL, CMake is used:

    mkdir build
    cd build
    cmake ..
    make
    ../bin/x86_64/Release/cross_kalman_mathtest

Run options
-----------

For a full list of available options, check with -h.

-m dictates how many experiments should be generated, whereas -n allows to
choose how many experiments to run over the generated ones, round robin. This is
convenient not to run out of space on accelerators.

Single precision is supported on all implementations. For NVIDIA or OpenCL,
search for DSP in the Makefile / CMakeLists.txt to enable it.

Different vector lengths are also supported, ie.

    # Compile with single precision and the highest available vector width on
    # the machine
    COMPILE_OPTS="-DSP" make

    # Compile with single precision and vector width 4
    COMPILE_OPTS="-DSP -DSTATIC_VECTOR_WIDTH=4" make

    # Compile with icc
    CXX=icc make

Enjoy burning CPU cycles.
