#pragma once

#include "Types.h"
#include "Similarity.h"

void update (
  FitNode& node,
  const int direction
);
