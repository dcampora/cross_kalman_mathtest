
#include "MatrixTypes.h"
#include "../fit/MatrixTypes.h"

Matrix1x5 operator*(const Matrix5x5& mA, const Matrix1x5& mB) {
  const double* A = (double*) &mA.fArray;
  const double* B = (double*) &mB.fArray;
  return Matrix1x5 {
    A[0]*B[0] + A[1]*B[1] + A[2]*B[2] + A[3]*B[3] + A[4]*B[4],
    A[5]*B[0] + A[6]*B[1] + A[7]*B[2] + A[8]*B[3] + A[9]*B[4],
    A[10]*B[0] + A[11]*B[1] + A[12]*B[2] + A[13]*B[3] + A[14]*B[4],
    A[15]*B[0] + A[16]*B[1] + A[17]*B[2] + A[18]*B[3] + A[19]*B[4],
    A[20]*B[0] + A[21]*B[1] + A[22]*B[2] + A[23]*B[3] + A[24]*B[4]
  };
}

double operator*(const Matrix1x5& mA, const Matrix1x5& mB) {
  const double* A = (double*) &mA.fArray;
  const double* B = (double*) &mB.fArray;
  return A[0]*B[0] + A[1]*B[1] + A[2]*B[2] + A[3]*B[3] + A[4]*B[4];
}

Matrix1x5 operator+(const Matrix1x5& mA, const Matrix1x5& mB) {
  const double* A = (double*) &mA.fArray;
  const double* B = (double*) &mB.fArray;
  return Matrix1x5 {
    A[0]+B[0],
    A[1]+B[1],
    A[2]+B[2],
    A[3]+B[3],
    A[4]+B[4]
  };
}

Matrix1x5 operator-(const Matrix1x5& mA, const Matrix1x5& mB) {
  const double* A = (double*) &mA.fArray;
  const double* B = (double*) &mB.fArray;
  return Matrix1x5 {
    A[0]-B[0],
    A[1]-B[1],
    A[2]-B[2],
    A[3]-B[3],
    A[4]-B[4]
  };
}

SymMatrix5x5 operator+(const SymMatrix5x5& mA, const SymMatrix5x5& mB) {
  const double* A = (double*) &mA.fArray;
  const double* B = (double*) &mB.fArray;
  return SymMatrix5x5 {
    A[0]+B[0], A[1]+B[1], A[2]+B[2], A[3]+B[3], A[4]+B[4],
    A[5]+B[5], A[6]+B[6], A[7]+B[7], A[8]+B[8], A[9]+B[9],
    A[10]+B[10], A[11]+B[11], A[12]+B[12], A[13]+B[13], A[14]+B[14]
  };
}

void operator+=(SymMatrix5x5& mA, const SymMatrix5x5& mB) {
  const double* B = (double*) &mB.fArray;
  for (int i=0; i<15; ++i)
    mA.fArray[i] += B[i];
}

Matrix5x5::Matrix5x5 (const VectorFit::TrackMatrixContiguous& mB) {
  for (int i=0; i<25; ++i) {
    fArray[i] = mB[i];
  }
}

SymMatrix5x5::SymMatrix5x5 (const VectorFit::TrackSymMatrixContiguous& mB) {
  for (int i=0; i<15; ++i) {
    fArray[i] = mB[i];
  }
}

void SymMatrix5x5::operator=(const VectorFit::TrackSymMatrix& mB) {
  for (int i=0; i<15; ++i) {
    fArray[i] = mB[i];
  }
}

void SymMatrix5x5::operator=(const VectorFit::TrackSymMatrixContiguous& mB) {
  for (int i=0; i<15; ++i) {
    fArray[i] = mB[i];
  }
}

Matrix1x5::Matrix1x5 (const VectorFit::TrackVectorContiguous& mB) {
  for (int i=0; i<5; ++i) {
    fArray[i] = mB[i];
  }
}

void Matrix1x5::operator=(const VectorFit::TrackVector& mB) {
  for (int i=0; i<5; ++i) {
    fArray[i] = mB[i];
  }
}

void Matrix1x5::operator=(const VectorFit::TrackVectorContiguous& mB) {
  for (int i=0; i<5; ++i) {
    fArray[i] = mB[i];
  }
}

Matrix1x5 operator- (
  const Matrix1x5& ma,
  const VectorFit::TrackVector& mb
) {
  return Matrix1x5 {
    ma[0] - mb[0],
    ma[1] - mb[1],
    ma[2] - mb[2],
    ma[3] - mb[3],
    ma[4] - mb[4]
  };
}
