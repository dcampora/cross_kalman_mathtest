#pragma once

#include "Types.h"
#include "Similarity.h"

void transportCovariance(
  const TrackMatrix& F,
  const TrackSymMatrix& origin,
  TrackSymMatrix& target
);

void predict(
  FitNode& node,
  const FitNode* const prevNode,
  const bool hasPrevNode,
  const int direction
);
