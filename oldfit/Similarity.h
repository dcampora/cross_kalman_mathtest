#pragma once

#include "Types.h"

void math_similarity_5_1(
  const double* Ci,
  const double* Fi,
  double* ti
);

void math_similarity_5_5 (
  const double* Ci,
  const double* Fi,
  double* ti
);

double math_update (
  double* X,
  double* C,
  const double* Xref,
  const double* H,
  double refResidual,
  double errorMeas2
);

bool math_average(
  const double* X1,
  const double* C1,
  const double* X2,
  const double* C2,
  double* X,
  double* C
);
