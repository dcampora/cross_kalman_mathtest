#include "Types.h"
#include "../fit/Types.h"

void operator+= (ChiSquare& c0, const ChiSquare& c1) {
  c0.m_chi2 += c1.m_chi2;
  c0.m_nDoF += c1.m_nDoF;
}

void StateVector::operator= (const VectorFit::StateVector& s) {
  m_parameters = s.m_parameters;
  m_z = s.m_z;
}

StateVector::StateVector (const VectorFit::StateVector& s) {
  m_parameters = s.m_parameters;
  m_z = s.m_z; 
}

Node::Node (const VectorFit::FitNode& node) 
  : m_type(node.m_type), 
  m_residual(*node.m_smoothState.m_residual), 
  m_errResidual(*node.m_smoothState.m_errResidual), 
  m_errMeasure(node.m_errMeasure), 
  m_projectionMatrix(node.m_projectionMatrix),
  m_refVector(node.m_refVector) {}

FitNode::FitNode (const VectorFit::FitNode& node)
  : Node(node),
  m_transportVector(node.m_transportVector),
  m_noiseMatrix(node.m_noiseMatrix),
  m_transportMatrix(node.m_forwardFit.m_transportMatrix),
  m_invertTransportMatrix(node.m_backwardFit.m_transportMatrix),
  m_refResidual(node.m_refResidual)
  {
    m_filteredState[Forward].m_stateVector = node.getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::StateVector>();
    m_filteredState[Forward].m_covariance = node.getState<VectorFit::Op::Forward, VectorFit::Op::Update, VectorFit::Op::Covariance>();

    m_filteredState[Backward].m_stateVector = node.getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::StateVector>();
    m_filteredState[Backward].m_covariance = node.getState<VectorFit::Op::Backward, VectorFit::Op::Update, VectorFit::Op::Covariance>();

    m_state.m_stateVector = node.getSmooth<VectorFit::Op::StateVector>();
    m_state.m_covariance = node.getSmooth<VectorFit::Op::Covariance>();
  }
