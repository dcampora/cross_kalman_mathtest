#include "Update.h"

// node.m_totalChi2[direction] = hasPrevNode ? prevNode->m_totalChi2[direction] : ChiSquare(0, -node.m_parent_nTrackParameters);

void update (
  FitNode& node,
  const int direction
) {
  // TODO: Should we have two different nodes - A forward, a backward node?
  
  // TODO: Do we need two objects to hold the states?
  node.m_filteredState[direction] = node.m_predictedState[direction];
  
  if (node.requireFilter()) {
    State& state = node.m_filteredState[direction];

    node.m_deltaChi2[direction] = ChiSquare(math_update(
      state.m_stateVector.fArray,
      state.m_covariance.fArray,
      node.m_refVector.m_parameters.fArray,
      node.m_projectionMatrix.fArray,
      node.m_refResidual,
      node.m_errMeasure * node.m_errMeasure
    ), 0.0);
  }
}
