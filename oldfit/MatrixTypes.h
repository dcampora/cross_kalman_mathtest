#pragma once

#include "CholeskyDecomp.h"

namespace VectorFit {
  class TrackVector;
  class TrackVectorContiguous;
  class TrackSymMatrix;
  class TrackSymMatrixContiguous;
  class TrackMatrixContiguous;
}

struct Matrix5x5 {
  double fArray[25];

  Matrix5x5 () = default;
  Matrix5x5 (const VectorFit::TrackMatrixContiguous& mB);

  double& operator()(const int x, const int y) {return fArray[x*5+y]; }
  double operator()(const int x, const int y) const {return fArray[x*5+y]; }
  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}
};

struct Matrix1x5 {
  double fArray[5];

  Matrix1x5 () = default;
  Matrix1x5 (double a0, double a1, double a2, double a3, double a4) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
  }
  Matrix1x5 (const VectorFit::TrackVectorContiguous& mB);

  double& operator()(const int x) {return fArray[x]; }
  double operator()(const int x) const {return fArray[x]; }
  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}

  void operator=(const VectorFit::TrackVector& mB);
  void operator=(const VectorFit::TrackVectorContiguous& mB);
};

struct SymMatrix5x5 {
  double fArray[15];

  SymMatrix5x5 () = default;
  SymMatrix5x5 (const SymMatrix5x5& _) = default;
  SymMatrix5x5 (const VectorFit::TrackSymMatrixContiguous& mB);
  SymMatrix5x5 (const double& a0, const double& a1, const double& a2, const double& a3, const double& a4,
    const double& a5, const double& a6, const double& a7, const double& a8, const double& a9,
    const double& a10, const double& a11, const double& a12, const double& a13, const double& a14) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
    fArray[5] = a5;
    fArray[6] = a6;
    fArray[7] = a7;
    fArray[8] = a8;
    fArray[9] = a9;
    fArray[10] = a10;
    fArray[11] = a11;
    fArray[12] = a12;
    fArray[13] = a13;
    fArray[14] = a14;
  }

  bool InvertChol() {
    ROOT::Math::CholeskyDecomp<double, 5> decomp(fArray);
    return decomp.Invert(fArray);
  }

  double& operator() (const int row, const int col) {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double operator()(const int row, const int col) const {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}

  void operator=(const VectorFit::TrackSymMatrix& mB);
  void operator=(const VectorFit::TrackSymMatrixContiguous& mB);
};

Matrix1x5 operator*(const Matrix5x5& mA, const Matrix1x5& mB);
Matrix1x5 operator+(const Matrix1x5& mA, const Matrix1x5& mB);
Matrix1x5 operator-(const Matrix1x5& mA, const Matrix1x5& mB);
SymMatrix5x5 operator+(const SymMatrix5x5& mA, const SymMatrix5x5& mB);
void operator+=(SymMatrix5x5& mA, const SymMatrix5x5& mB);
double operator*(const Matrix1x5& mA, const Matrix1x5& mB);
Matrix1x5 operator-(const Matrix1x5& ma, const VectorFit::TrackVector& mb);

typedef Matrix1x5 TrackVector;
typedef Matrix1x5 TrackProjectionMatrix;
typedef Matrix5x5 TrackMatrix;
typedef SymMatrix5x5 TrackSymMatrix;
