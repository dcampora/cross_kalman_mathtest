#pragma once

// Print function for result
template<unsigned long N>
void printState (
  const unsigned& element,
  const std::array<PRECISION, 5*N>& ps,
  const std::array<PRECISION, 15*N>& pc,
  const std::array<PRECISION, 5*N>& us,
  const std::array<PRECISION, 15*N>& uc,
  const std::array<PRECISION, N>& chi2
) {
  // Print first result
  std::cout << "ps: {";
  for (unsigned i=0; i<5; ++i) {
    std::cout << ps[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "pc: {";
  for (unsigned i=0; i<15; ++i) {
    std::cout << pc[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "x: {";
  for (unsigned i=0; i<5; ++i) {
    std::cout << us[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "c: {";
  for (unsigned i=0; i<15; ++i) {
    std::cout << uc[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "chi2: " << chi2[element] << std::endl;
}

template<unsigned long N>
void printSmoothState (
  const unsigned& element,
  const std::array<PRECISION, 5*N>& us1,
  const std::array<PRECISION, 15*N>& uc1,
  const std::array<PRECISION, 5*N>& us2,
  const std::array<PRECISION, 15*N>& uc2,
  const std::array<PRECISION, 5*N>& us,
  const std::array<PRECISION, 15*N>& uc,
  const std::array<PRECISION, N>& res,
  const std::array<PRECISION, N>& errRes
) {
  // Print first result
  std::cout << "us1: {";
  for (unsigned i=0; i<5; ++i) {
    std::cout << us1[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "uc1: {";
  for (unsigned i=0; i<15; ++i) {
    std::cout << uc1[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "us2: {";
  for (unsigned i=0; i<5; ++i) {
    std::cout << us2[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "uc2: {";
  for (unsigned i=0; i<15; ++i) {
    std::cout << uc2[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "us: {";
  for (unsigned i=0; i<5; ++i) {
    std::cout << us[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl << "uc: {";
  for (unsigned i=0; i<15; ++i) {
    std::cout << uc[i*N + element] << ", ";
  }
  std::cout << "}" << std::endl
    << "res: " << res[element] << std::endl
    << "errRes: " << errRes[element] << std::endl;
}
