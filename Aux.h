#pragma once

#include <chrono>
#include <random>
#include <array>
#include "utilities/Timer.h"
#include "fit/VectorConfiguration.h"
#include "fit/FitMath.h"

template<long unsigned N>
void add (
  PRECISION& d,
  const std::array<PRECISION, N>& v
) {
  d = std::accumulate(v.begin(), v.end(), d);
}

template<long unsigned W, long unsigned N>
void vassign (
  PRECISION* const m,
  const std::array<PRECISION, N>& v
) {
  for (unsigned i=0; i<N; ++i) m[i*W] = v[i];
}

template<long unsigned N>
std::array<PRECISION, N> generate (
  std::mt19937& mt,
  std::uniform_real_distribution<PRECISION>& dist
) {
  _aligned std::array<PRECISION, N> m;
  for (unsigned i=0; i<N; ++i) {
    m[i] = dist(mt);
  }

  return m;
}
