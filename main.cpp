#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include "Vec.h"

#ifdef SP
#pragma message ("Compiling for vector width " TO_STRING(VECTOR_WIDTH) ", single precision")
#else
#pragma message ("Compiling for vector width " TO_STRING(VECTOR_WIDTH) ", double precision")
#endif

int main (const int argc, char * const *argv) {
  unsigned genExperiments = 1024;
  unsigned numExperiments = 1024;
  double minValue = 1.0;
  double maxValue = 1000.0;
  unsigned mask = 0x01;
  int printExp = -1;

  char c;
  while (true) {
    c = getopt (argc, argv, "m:n:k:p:");
    if (c == -1 or c == 255 /* power8 */) break;
    switch (c) {
      case 'm':
        genExperiments = atoi(optarg);
        break;
      case 'n':
        numExperiments = atoi(optarg);
        break;
      case 'k':
        mask = atoi(optarg);
        break;
      case 'p':
        printExp = atoi(optarg);
        break;
      case '?':
        std::cout << "test [-m generate=1024] [-n numExperimentsToRun=1024] "
          << "[-k mask=1 (1 fit, 2 smooth)] [-p printExp=-1]" << std::endl;
        return 0;
      default:
        break;
    }
  }

  // Check numExperiments is % VECTOR_WIDTH
  if (numExperiments % VECTOR_WIDTH || genExperiments % VECTOR_WIDTH) {
    std::cout << "Warning: generated or numExperiments is not divisible by VECTOR_WIDTH" << std::endl;
    genExperiments = genExperiments - (genExperiments % VECTOR_WIDTH);
    numExperiments = numExperiments - (numExperiments % VECTOR_WIDTH);
  }

  genExperiments = std::min(numExperiments, genExperiments);

  if (numExperiments == 0 or genExperiments == 0) {
    std::cout << "Requested 0 experiments to generate or run, exiting..." << std::endl;
    exit(1);
  } else {
    std::cout << "Running " << numExperiments << " experiments..." << std::endl;
  }

  // Generate some reproducible random data
  // std::random_device rd;
  std::mt19937 mt (0);
  std::uniform_real_distribution<PRECISION> dist (minValue, maxValue);

  // dist(mt)
  // Run for a number of iterations
  Timer t, tshuffle;
  double foo = 0.0;
  VectorFit::VectorTest<VECTOR_WIDTH> test (mt, dist);

  if (mask & 0x01) {
    foo += test.fit(genExperiments, numExperiments, printExp);
  }
  if (mask & 0x02) {
    foo += test.smoother(genExperiments, numExperiments, printExp);
  }

  t = test.t;
  tshuffle = test.tshuffle;

  std::cout << "Checksum: " << foo << std::endl;
  std::cout << "Time shuffle: " << tshuffle.get() << std::endl;
  std::cout << "Time: " << t.get() << std::endl;

  return 0;
}
