#pragma once

#include <array>
#include "VectorConfiguration.h"
#include "TypesAux.h"
#include "../oldfit/CholeskyDecomp.h"

// Oldfit types, just to do the conversion to the new types
typedef FitNode FitNodeAOS;
typedef TrackVector TrackVectorAOS;
typedef TrackMatrix TrackMatrixAOS;
typedef TrackSymMatrix TrackSymMatrixAOS;

namespace VectorFit {

struct TrackVectorContiguous {
  _aligned std::array<PRECISION, 5> fArray;

  TrackVectorContiguous () = default;
  TrackVectorContiguous (PRECISION a0, PRECISION a1, PRECISION a2, PRECISION a3, PRECISION a4) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
  }
  TrackVectorContiguous (const TrackVectorAOS& mB) {
    for (int i=0; i<5; ++i) {
      fArray[i] = mB[i];
    }    
  }

  PRECISION& operator() (const int x) {return fArray[x]; }
  PRECISION operator() (const int x) const {return fArray[x]; }
  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  void operator= (const TrackVectorAOS& mB) {
    for (int i=0; i<5; ++i) {
      fArray[i] = mB[i];
    }
  }
};

struct TrackMatrixContiguous {
  _aligned std::array<PRECISION, 25> fArray;

  PRECISION& operator() (const int x, const int y) {return fArray[x*5+y]; }
  PRECISION operator() (const int x, const int y) const {return fArray[x*5+y]; }
  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  void operator= (const TrackMatrixAOS& mB) {
    for (int i=0; i<25; ++i) {
      fArray[i] = mB[i];
    }
  }
};

struct TrackSymMatrixContiguous {
  _aligned std::array<PRECISION, 15> fArray;

  TrackSymMatrixContiguous () = default;
  TrackSymMatrixContiguous (const TrackSymMatrixAOS& mB) {
    for (int i=0; i<15; ++i) {
      fArray[i] = mB[i];
    }    
  }

  bool InvertChol() {
    ROOT::Math::CholeskyDecomp<PRECISION, 5> decomp(fArray.data());
    return decomp.Invert(fArray.data());
  }

  PRECISION& operator() (const int row, const int col) {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  PRECISION operator() (const int row, const int col) const {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  void operator=(const TrackSymMatrixAOS& mB) {
    for (int i=0; i<15; ++i) {
      fArray[i] = mB[i];
    }
  }
};

struct TrackVector {
  PRECISION* m_basePointer = 0x0;
  TrackVector () = default;
  TrackVector (PRECISION* m_basePointer) : m_basePointer(m_basePointer) {}
  /**
   * @brief      Copies v into its state
   *             Assumes m_basePointer is well defined
   */
  inline void copy (const TrackVector& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void copy (const TrackVectorContiguous& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator= (const TrackVectorContiguous& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator+= (const TrackVectorContiguous& v) {
    for (int i=0; i<5; ++i) {
      this->operator[](i) += v[i];
    }
  }
  // Request item i
  inline PRECISION& operator[] (const unsigned int i) { return m_basePointer[i * VECTOR_WIDTH]; }
  inline PRECISION operator[] (const unsigned int i) const { return m_basePointer[i * VECTOR_WIDTH]; }
  inline void setBasePointer (const TrackVector& v) { m_basePointer = v.m_basePointer; }
  inline void setBasePointer (PRECISION* m_basePointer) { this->m_basePointer = m_basePointer; }
};

struct TrackSymMatrix {
  PRECISION* m_basePointer = 0x0;
  TrackSymMatrix () = default;
  TrackSymMatrix (PRECISION* m_basePointer) : m_basePointer(m_basePointer) {}
  /**
   * @brief      Copies v into its state
   *             Assumes m_basePointer is well defined
   */
  inline void copy (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void copy (const TrackSymMatrixContiguous& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = v[i];
    }
  }
  inline void operator+= (const TrackSymMatrix& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) += v[i];
    }
  }
  inline void operator+= (const TrackSymMatrixContiguous& v) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) += v[i];
    }
  }
  // Request item i
  inline PRECISION& operator[] (const unsigned int i) {
    return m_basePointer[i * VECTOR_WIDTH];
  }
  inline PRECISION operator[] (const unsigned int i) const {
    return m_basePointer[i * VECTOR_WIDTH];
  }
  inline PRECISION& operator()(const int row, const int col) {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  inline PRECISION operator()(const int row, const int col) const {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  inline void setBasePointer (const TrackSymMatrix& m) { m_basePointer = m.m_basePointer; }
  inline void setBasePointer (PRECISION* m_basePointer) { this->m_basePointer = m_basePointer; }

  inline void operator= (const TrackSymMatrixAOS& mB) {
    for (int i=0; i<15; ++i) {
      this->operator[](i) = mB[i];
    }
  }
};

// Some operators
inline TrackVectorContiguous operator-(const TrackVectorContiguous& mA, const TrackVector& mB) {
  const PRECISION* A = (PRECISION*) &mA.fArray;
  return TrackVectorContiguous {
    A[0]-mB[0],
    A[1]-mB[1],
    A[2]-mB[2],
    A[3]-mB[3],
    A[4]-mB[4]
  };
}

}
