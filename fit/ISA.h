#pragma once

#include "VectorConfiguration.h"

namespace ISAs {
  struct DEFAULT {
    template <class F>
    static constexpr std::size_t card() { return 1; }
  };
  struct SCALAR {
    template <class F>
    static constexpr std::size_t card() { return 1; }
  };
  struct SSE {
    template <class F>
    static constexpr std::size_t card() { return 16 / sizeof(F); }
  };
  struct AVX {
    template <class F>
    static constexpr std::size_t card() { return 32 / sizeof(F); }
  };
  struct AVX512 {
    template <class F>
    static constexpr std::size_t card() { return 64 / sizeof(F); }
  };

#ifdef SP

#if VECTOR_WIDTH == 16u
  using CURRENT = AVX512;
#elif VECTOR_WIDTH == 8u
  using CURRENT = AVX;
#elif VECTOR_WIDTH == 4u
  using CURRENT = SSE;
#else
  using CURRENT = SCALAR;
#endif

#else

#if VECTOR_WIDTH == 8u
  using CURRENT = AVX512;
#elif VECTOR_WIDTH == 4u
  using CURRENT = AVX;
#elif VECTOR_WIDTH == 2u
  using CURRENT = SSE;
#else
  using CURRENT = SCALAR;
#endif

#endif
}
