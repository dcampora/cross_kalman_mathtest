#pragma once

#include "Types.h"
#include "Scheduler.h"
#include "FitMath.h"
#include "ArrayGen.h"

namespace VectorFit {

template<class T>
inline void transportCovariance (
  const TrackMatrixContiguous& tm,
  const T& uc,
  TrackSymMatrix& pc
) {
  bool isLine = tm(0,4)==0;
  if (!isLine) {
    FitMathNonVec::similarity_5_5(tm, uc, pc);
  } else {
    pc.copy(uc);

    if (tm(0,2) != 0 or tm(1,3) != 0) {
      pc(0,0) += 2 * uc(2,0) * tm(0,2) + uc(2,2) * tm(0,2) * tm(0,2);
      pc(2,0) += uc(2,2) * tm(0,2);
      pc(1,1) += 2 * uc(3,1) * tm(1,3) + uc(3,3) * tm(1,3) * tm(1,3);
      pc(3,1) += uc(3,3) * tm(1,3);
      pc(1,0) += uc(2,1) * tm(0,2) + uc(3,0) * tm(1,3) + uc(3,2) * tm(0,2) * tm(1,3);
      pc(2,1) += uc(3,2) * tm(1,3);
      pc(3,0) += uc(3,2) * tm(0,2);
    }
  }
}

template<class T, bool U>
inline void predict (
  FitNode& node,
  const FitNode& prevnode
);

template<>
inline void predict<Op::Forward, false> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.getState<Op::Forward, Op::Update, Op::StateVector>().copy(node.m_refVector.m_parameters);
  node.getState<Op::Forward, Op::Update, Op::Covariance>().copy(prevnode.getState<Op::Forward, Op::Update, Op::Covariance>());
}

template<>
inline void predict<Op::Forward, true> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.getState<Op::Forward, Op::Update, Op::StateVector>().copy(node.m_transportVector);
  for (int i=0; i<5; ++i) {
    for (int j=0; j<5; ++j) {
      node.getState<Op::Forward, Op::Update, Op::StateVector>()[i] += 
        node.getFit<Op::Forward>().m_transportMatrix[5*i + j] *
        prevnode.getState<Op::Forward, Op::Update, Op::StateVector>()[j];
    }
  }

  transportCovariance(
    node.getFit<Op::Forward>().m_transportMatrix,
    prevnode.getState<Op::Forward, Op::Update, Op::Covariance>(),
    node.getState<Op::Forward, Op::Update, Op::Covariance>());
  node.getState<Op::Forward, Op::Update, Op::Covariance>() += node.m_noiseMatrix;
}

template<>
inline void predict<Op::Backward, false> (
  FitNode& node,
  const FitNode& prevnode
) {
  node.getState<Op::Backward, Op::Update, Op::StateVector>().copy(node.getState<Op::Forward, Op::Update, Op::StateVector>());
  node.getState<Op::Backward, Op::Update, Op::Covariance>().copy(prevnode.getState<Op::Backward, Op::Update, Op::Covariance>());
}

template<>
inline void predict<Op::Backward, true> (
  FitNode& node,
  const FitNode& prevnode
) {
  TrackVectorContiguous temp_sub;
  for (int i=0; i<5; ++i) {
    temp_sub[i] = prevnode.getState<Op::Backward, Op::Update, Op::StateVector>()[i] - prevnode.m_transportVector[i];
  };

  for (int i=0; i<5; ++i) {
    node.getState<Op::Backward, Op::Update, Op::StateVector>()[i] = prevnode.getFit<Op::Backward>().m_transportMatrix[5*i] * temp_sub[0];
    for (int j=1; j<5; ++j) {
      node.getState<Op::Backward, Op::Update, Op::StateVector>()[i] += prevnode.getFit<Op::Backward>().m_transportMatrix[5*i + j] * temp_sub[j];
    }
  }

  TrackSymMatrixContiguous temp_cov;
  for (int i=0; i<15; ++i) {
    temp_cov[i] = prevnode.getState<Op::Backward, Op::Update, Op::Covariance>()[i] + prevnode.m_noiseMatrix[i];
  }

  transportCovariance (
    prevnode.getFit<Op::Backward>().m_transportMatrix,
    temp_cov,
    node.getState<Op::Backward, Op::Update, Op::Covariance>()
  );
}

template<class T>
struct predict_vec {
  template<unsigned W>
  static inline void op (
    const std::array<PRECISION, 25*W>& tm,
    const std::array<PRECISION, 15*W>& nm,
    const std::array<PRECISION, 5*W>& tv,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  );
};

// Vectorised predicts that assume data is already prepared, aligned and so on

template<>
struct predict_vec<Op::Forward> {
  template<unsigned W>
  static inline void op (
    const std::array<PRECISION, 25*W>& tm,
    const std::array<PRECISION, 15*W>& nm,
    const std::array<PRECISION, 5*W>& tv,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    FitMath<Op::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Op::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

template<>
struct predict_vec<Op::Backward> {
  template<unsigned W>
  static inline void op (
    const std::array<PRECISION, 25*W>& tm,
    const std::array<PRECISION, 15*W>& nm,
    const std::array<PRECISION, 5*W>& tv,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    FitMath<Op::Backward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    FitMath<Op::Backward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

}
