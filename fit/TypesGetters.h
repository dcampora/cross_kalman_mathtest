namespace VectorFit {

// ---------------
// get methods
// ---------------

template<>
inline const FitParameters& FitNode::getFit<Op::Backward> () const {
  return m_backwardFit;
}

template<>
inline const FitParameters& FitNode::getFit<Op::Forward> () const {
  return m_forwardFit;
}

template<>
inline const TrackVector& FitNode::getState<Op::Forward, Op::Update, Op::StateVector> () const {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
inline const TrackSymMatrix& FitNode::getState<Op::Forward, Op::Update, Op::Covariance> () const {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
inline const TrackVector& FitNode::getState<Op::Backward, Op::Update, Op::StateVector> () const {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
inline const TrackSymMatrix& FitNode::getState<Op::Backward, Op::Update, Op::Covariance> () const {
  return m_backwardFit.m_states.m_updatedCovariance;
}

template<>
inline const TrackVector& FitNode::getSmooth<Op::StateVector> () const {
  return m_smoothState.m_state;
}

template<>
inline const TrackSymMatrix& FitNode::getSmooth<Op::Covariance> () const {
  return m_smoothState.m_covariance;
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
inline FitParameters& FitNode::getFit<Op::Forward> () {
  return m_forwardFit;
}

template<>
inline FitParameters& FitNode::getFit<Op::Backward> () {
  return m_backwardFit;
}

template<>
inline TrackVector& FitNode::getState<Op::Forward, Op::Update, Op::StateVector> () {
  return m_forwardFit.m_states.m_updatedState;
}

template<>
inline TrackSymMatrix& FitNode::getState<Op::Forward, Op::Update, Op::Covariance> () {
  return m_forwardFit.m_states.m_updatedCovariance;
}

template<>
inline TrackVector& FitNode::getState<Op::Backward, Op::Update, Op::StateVector> () {
  return m_backwardFit.m_states.m_updatedState;
}

template<>
inline TrackSymMatrix& FitNode::getState<Op::Backward, Op::Update, Op::Covariance> () {
  return m_backwardFit.m_states.m_updatedCovariance;
}

template<>
inline TrackVector& FitNode::getSmooth<Op::StateVector> () {
  return m_smoothState.m_state;
}

template<>
inline TrackSymMatrix& FitNode::getSmooth<Op::Covariance> () {
  return m_smoothState.m_covariance;
}

}