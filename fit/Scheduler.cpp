#include "Scheduler.h"

namespace VectorFit {

std::vector<std::tuple<uint16_t, uint16_t, uint16_t, std::array<SchItem, VECTOR_WIDTH>>>
DumbStaticScheduler::generate (
  std::vector<Track, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>& tracks,
  const bool showSchedule
) {
  // Dumb Simple Static Scheduler
  // Order tracks by size, from more to less nodes
  std::vector<std::reference_wrapper<Track>> ordered_ntracks (tracks.begin(), tracks.end());
  std::sort(ordered_ntracks.begin(), ordered_ntracks.end(), [] (const Track& t0, const Track& t1) {
    const unsigned size0 = t0.m_nodes.size() - t0.m_forwardUpstream - t0.m_backwardUpstream;
    const unsigned size1 = t1.m_nodes.size() - t1.m_forwardUpstream - t1.m_backwardUpstream;

    return size0 > size1;
  });
  
  // Reserve a bunch of things
  std::vector<std::tuple<uint16_t, uint16_t, uint16_t, std::array<SchItem, VECTOR_WIDTH>>> forwardScheduler;
  forwardScheduler.reserve(1000);
  auto trackIterator = ordered_ntracks.begin();
  
  std::array<SchItem, VECTOR_WIDTH> schArray;
  std::vector<unsigned> slots (VECTOR_WIDTH);
  std::iota(slots.begin(), slots.end(), 0);
  
  uint16_t inputMask  = 0x0000;
  uint16_t outputMask = 0x0000;
  uint16_t actionMask = 0xFFFF;

  while (trackIterator != ordered_ntracks.end()) {
    const unsigned slot = slots.back();
    slots.pop_back();
    Track& track = *trackIterator;
  
    // Calculate input mask, generate SchItem
    inputMask |= 1 << slot;
    schArray[slot] = SchItem (
      &track.m_nodes,
      track.m_forwardUpstream,
      track.m_forwardUpstream + 1,
      track.m_backwardUpstream,
      track.m_index
    );

    while (slots.empty()) {
      // Liberate slots and calculate output mask
      outputMask = 0x0000;
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        SchItem& s = schArray[i];
        
        if (s.node + 1 == s.lastnode) {
          outputMask |= 1 << i;
          slots.push_back(i);
        }
      }

      // Add an item to the scheduler
      forwardScheduler.emplace_back(std::make_tuple(inputMask, outputMask, actionMask, schArray));

      // Reinitialize
      inputMask = 0x0000;
      
      std::for_each (schArray.begin(), schArray.end(), [] (SchItem& s) {
        if (s.node + 1 != s.lastnode) {
          ++s.prevnode;
          ++s.node;
        }
      });
    }

    ++trackIterator;
  }

  // Iterations with no full vectors
  if (slots.size() < VECTOR_WIDTH) {
    std::vector<int> processingIndices (VECTOR_WIDTH);
    std::iota(processingIndices.begin(), processingIndices.end(), 0);
    for (auto it=slots.rbegin(); it!=slots.rend(); ++it) {
      processingIndices.erase(processingIndices.begin() + *it);
    }

    // We will finish once all slots are used up
    auto processingIndicesEnd = processingIndices.end();
    while (processingIndicesEnd != processingIndices.begin()) {

      actionMask = 0x0000;
      outputMask = 0x0000;
      std::for_each (processingIndices.begin(), processingIndicesEnd, [&] (const int& i) {
        SchItem& s = schArray[i];
        actionMask |= 1 << i;
        
        if (s.node + 1 == s.lastnode) {
          outputMask |= 1 << i;
        }
      });

      // Add an item to the scheduler
      forwardScheduler.emplace_back(std::make_tuple(inputMask, outputMask, actionMask, schArray));

      // Update the input mask
      inputMask = 0x0000;

      // Update all tracks information and check for finished tracks
      processingIndicesEnd = std::remove_if(processingIndices.begin(), processingIndicesEnd, [&] (const int& i) {
        // Advance to next node
        SchItem& s = schArray[i];

        if (s.node + 1 == s.lastnode) return true;
        
        ++s.prevnode;
        ++s.node;

        return false;
      });
    }
  }

  // Print what we scheduled
  if (showSchedule) {
    for (unsigned i=0; i<forwardScheduler.size(); ++i) {
      auto& item = forwardScheduler[i];
      std::cout << "#" << i << ": ";

      print(std::get<0>(item));
      print(std::get<1>(item));
      print(std::get<2>(item));
      auto& schArray = std::get<3>(item);

      std::cout << "{ ";

      for (unsigned j=0; j<schArray.size(); ++j) {
        std::cout << schArray[j].trackIndex << "-" << schArray[j].node->m_index << " ";
      }
      std::cout << "}" << std::endl;
    }
  }

  return forwardScheduler;
}

}
