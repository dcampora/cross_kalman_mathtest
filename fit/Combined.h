#pragma once

#include <array>
#include "Predict.h"
#include "Update.h"

namespace VectorFit {

// Initialise
template<class T>
inline void fit (
  FitNode& node,
  const TrackSymMatrixContiguous& covariance
) {
  node.getState<T, Op::Update, Op::StateVector>().copy(node.m_refVector.m_parameters);
  node.getState<T, Op::Update, Op::Covariance>().copy(covariance);

  update<T>(node);
}

template<class T, bool U>
inline void fit (
  FitNode& node,
  const FitNode& prevnode
) {
  predict<T, U>(node, prevnode);
  update<T>(node);
}

template<class T>
struct fit_vec {
  template<unsigned W>
  static inline void op (
    const std::array<PRECISION, 25*W>& tm,
    const std::array<PRECISION, 15*W>& nm,
    const std::array<PRECISION, 5*W>& tv,
    const std::array<PRECISION, 5*W>& Xref,
    const std::array<PRECISION, 5*W>& H,
    const std::array<PRECISION, W>& refResidual,
    const std::array<PRECISION, W>& errorMeas2,
    fp_ptr_64_const last_us,
    fp_ptr_64_const last_uc,
    fp_ptr_64 us,
    fp_ptr_64 uc,
    fp_ptr_64 chi2
  ) {
    predict_vec<T>::template op<W> (
      tm,
      nm,
      tv,
      last_us,
      last_uc,
      us,
      uc
    );

    update_vec<W> (
      Xref,
      H,
      refResidual,
      errorMeas2,
      us,
      uc,
      chi2
    );
  }
};

template<long unsigned W>
inline uint16_t full_smoother_vec (
  const std::array<PRECISION, 5*W>& pm,
  const std::array<PRECISION, 5*W>& pa,
  const std::array<PRECISION, W>& em,
  const std::array<PRECISION, W>& rr,
  fp_ptr_64_const s1,
  fp_ptr_64_const c1,
  fp_ptr_64_const s2,
  fp_ptr_64_const c2,
  fp_ptr_64 ss,
  fp_ptr_64 sc,
  fp_ptr_64 res,
  fp_ptr_64 errRes
) {
  uint16_t r = FitMathCommon<W>::average (
    s1,
    s2,
    c1,
    c2,
    ss,
    sc
  );

  FitMathCommon<W>::updateResiduals (
    pm,
    pa,
    em,
    rr,
    ss,
    sc,
    res,
    errRes
  );

  return r;
}

}
