#pragma once

#include <vector>
#include <functional>
#include "Store.h"
#include "Cpp14Compat.h"
#include "VectorConfiguration.h"
#include "MatrixTypes.h"
#include "TypesAux.h"

// Names for templates
namespace VectorFit {

namespace Op {
  class Backward;
  class Forward;
  class Predict;
  class Update;
  class Smooth;
  class StateVector;
  class Covariance;
}

struct SmoothState {
  PRECISION* m_basePointer = 0x0;
  TrackVector m_state;
  TrackSymMatrix m_covariance;
  PRECISION* m_residual;
  PRECISION* m_errResidual;

  SmoothState () = default;
  SmoothState (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_state = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_covariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_residual = p; p += VECTOR_WIDTH;
    m_errResidual = p;
  }

  void setBasePointer (const SmoothState& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const SmoothState& s) {
    m_state.copy(s.m_state);
    m_covariance.copy(s.m_covariance);
    *m_residual = *s.m_residual;
    *m_errResidual = *s.m_errResidual;
  }
};

/**
 * @brief      Our flamant AOSOA
 */
struct States {
  PRECISION* m_basePointer = 0x0;
  TrackVector m_updatedState;
  TrackSymMatrix m_updatedCovariance;
  PRECISION* m_chi2;

  States () = default;
  States (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_updatedState   = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_updatedCovariance   = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_chi2 = p;
  }

  void setBasePointer (const States& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const States& s) {
    m_updatedState.copy(s.m_updatedState);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }

  States operator++ () {
    setBasePointer(m_basePointer + 21 * VECTOR_WIDTH);
    return *this;
  }

  States operator-- () {
    setBasePointer(m_basePointer - 21 * VECTOR_WIDTH);
    return *this;
  }
};

struct StateVector {
  _aligned TrackVectorContiguous m_parameters;
  PRECISION m_z ;

  StateVector () = default;
  StateVector (const StateVectorAOS& state);
};

struct FitParameters {
  _aligned TrackMatrixContiguous m_transportMatrix;
  States m_states;

  FitParameters (const FitParameters& node) = default;
  FitParameters (const FitNodeAOS& node, const int& direction);
};

struct Node {
  _aligned TrackVectorContiguous m_projectionMatrix;
  Type m_type;
  StateVector m_refVector;
  PRECISION m_errMeasure;
  int* m_measurement;

  Node () = default;
  Node (const Node& node) = default;
  Node (const FitNodeAOS& node);
};

struct FitNode : public Node {
  _aligned TrackVectorContiguous m_transportVector;
  _aligned TrackSymMatrixContiguous m_noiseMatrix;
  FitParameters m_forwardFit;
  FitParameters m_backwardFit;
  SmoothState m_smoothState;
  PRECISION m_refResidual;
  unsigned m_index;

  FitNode () = default;
  FitNode (const FitNode& node) = default;
  FitNode (const FitNodeAOS& node);
  FitNode (const FitNodeAOS& node, const unsigned& index);

  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
  inline const U& getState () const;
  template<class R, class S, class T,
    class U = typename std::conditional<std::is_same<T, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
  inline U& getState ();

  template<class R,
    class U = typename std::conditional<std::is_same<R, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
  const U& getSmooth () const;
  template<class R,
    class U = typename std::conditional<std::is_same<R, Op::Covariance>::value, TrackSymMatrix, TrackVector>::type>
  U& getSmooth ();

  template<class R>
  inline const FitParameters& getFit () const;
  template<class R>
  inline FitParameters& getFit ();

  template<class R>
  inline const PRECISION& getChi2 () const {
    return *(getFit<R>().m_states.m_chi2);
  }
  template<class R>
  inline PRECISION& getChi2 () {
    return *(getFit<R>().m_states.m_chi2);
  }
};

struct Track {
  std::vector<FitNode, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> m_nodes;

  // Active measurements from this node
  unsigned m_forwardUpstream;
  unsigned m_backwardUpstream;

  // Chi square of track
  PRECISION m_forwardFitChi2 = 0.0;
  PRECISION m_backwardFitChi2 = 0.0;
  
  // Some other things
  unsigned m_index;
  int m_ndof = 0;
  int m_parent_nTrackParameters;

  // Initial covariances
  TrackSymMatrixContiguous m_initialForwardCovariance;
  TrackSymMatrixContiguous m_initialBackwardCovariance;

  Track () = default;
  Track (const Track& track) = default;
  Track (
    const std::vector<FitNodeAOS>& track,
    const unsigned& trackNumber,
    GrowingMemManager& memManager
  );
};

}

#include "TypesGetters.h"
