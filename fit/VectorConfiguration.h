#pragma once

#define TO_STRING(s) WRAPPED_TO_STRING(s)
#define WRAPPED_TO_STRING(s) #s

// Set compiling values
#ifdef SP

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__)
  #define VECTOR_WIDTH 16u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 8u
#elif defined(__SSE__)
  #define VECTOR_WIDTH 4u
#else
  #define VECTOR_WIDTH 1u
#endif

#else

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__)
  #define VECTOR_WIDTH 8u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 4u
#elif defined(__SSE__)
  #define VECTOR_WIDTH 2u
#else
  #define VECTOR_WIDTH 1u
#endif

#endif

#if VECTOR_WIDTH > 1
  // Include vectorclass
  #define MAX_VECTOR_SIZE 512
  #include "../vectorclass/vectorclass.h"
#endif

#include "TypesAux.h"

template <unsigned W> struct Vectype { using type = PRECISION; using booltype = bool; };

#ifdef SP

#if defined(__AVX512F__)
template<> struct Vectype<16> { using type = Vec16f; using booltype = Vec16fb; };
#endif
#if defined(__AVX__)
template<> struct Vectype<8> { using type = Vec8f; using booltype = Vec8fb; };
#endif
#if defined(__SSE__)
template<> struct Vectype<4> { using type = Vec4f; using booltype = Vec4fb; };
#endif

#else

#if defined(__AVX512F__)
template<> struct Vectype<8> { using type = Vec8d; using booltype = Vec8db; };
#endif
#if defined(__AVX__)
template<> struct Vectype<4> { using type = Vec4d; using booltype = Vec4db; };
#endif
#if defined(__SSE__)
template<> struct Vectype<2> { using type = Vec2d; using booltype = Vec2db; };
#endif

#endif
