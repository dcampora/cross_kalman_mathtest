#pragma once

#include <vector>
#include "AlignedAllocator.h"
#include "VectorConfiguration.h"
#include "assert.h"

namespace VectorFit {

struct MemManager {
  std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> backend;
  int width = 21;
  int currentElement = 0;
  int vectorNumber = 0;

  MemManager () = default;
  MemManager (const MemManager&) = default;
  MemManager (MemManager&&) = default;
  MemManager& operator= (const MemManager&) = default;
  MemManager& operator= (MemManager&&) = default;

  MemManager (
    const int& width,
    const int& numberOfNodes
  ) : backend(numberOfNodes * width),
  width(width)
  {}

  void reset () {
    // this->width = width;
    // backend.resize(numberOfNodes * width);
    currentElement = 0;
    vectorNumber = 0;
  }

  PRECISION* getNewVector () {
    if (currentElement != 0) {
      // We were requested a new vector,
      // but we are not on the initial element
      // 
      // Check if we still have space for another vector,
      // otherwise TODO (for the moment resize)
      ++vectorNumber;
      currentElement = 0;
    }
    assert(backend.size() > (vectorNumber + 1) * VECTOR_WIDTH * width);

    PRECISION* d = backend.data() + vectorNumber * VECTOR_WIDTH * width;
    ++vectorNumber;
    return d;
  }

  PRECISION* getNextElement () {
    PRECISION* d = backend.data() + vectorNumber * VECTOR_WIDTH * width + currentElement;
    ++currentElement;
    if (currentElement == VECTOR_WIDTH) {
      ++vectorNumber;
      currentElement = 0;

      assert(backend.size() > (vectorNumber + 1) * VECTOR_WIDTH * width);
    }
    return d;
  }

  PRECISION* getLastVector () {
    assert(vectorNumber >= 1);

    return backend.data() + (vectorNumber - 1) * VECTOR_WIDTH * width;
  }

  PRECISION* getFirstVector () {
    return backend.data();
  }
};

struct GrowingMemManager {
  int currentBackend;
  int currentSize;
  int currentElement;
  int width;
  int vectorNumber;
  std::vector<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>> backends;

  GrowingMemManager (
    const int& width
  ) : width(width) {
    vectorNumber = 0;
    currentBackend = 0;
    currentElement = 0;
    currentSize = 41 * 1024;
    backends.emplace_back(currentSize * width);
  }

  PRECISION* getNextElement () {
    PRECISION* d = backends[currentBackend].data() + vectorNumber * VECTOR_WIDTH * width + currentElement;
    ++currentElement;
    if (currentElement == VECTOR_WIDTH) {
      ++vectorNumber;
      currentElement = 0;

      if (backends[currentBackend].size() < (vectorNumber + 1) * VECTOR_WIDTH * width) {
        vectorNumber = 0;
        ++currentBackend;
        backends.emplace_back(currentSize * width);
      }
    }
    return d;
  }
};

}
