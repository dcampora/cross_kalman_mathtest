#pragma once

#include "Types.h"
#include "FitMath.h"

namespace VectorFit {

template<bool F, bool B>
inline void smoother (
  FitNode& node
);

template<>
inline void smoother<false, true> (
  FitNode& node
) {
  node.getSmooth<Op::StateVector>().setBasePointer(node.getState<Op::Backward, Op::Update, Op::StateVector>());
  node.getSmooth<Op::Covariance>().setBasePointer(node.getState<Op::Backward, Op::Update, Op::Covariance>());
}

template<>
inline void smoother<true, false> (
  FitNode& node
) {
  node.getSmooth<Op::StateVector>().setBasePointer(node.getState<Op::Forward, Op::Update, Op::StateVector>());
  node.getSmooth<Op::Covariance>().setBasePointer(node.getState<Op::Forward, Op::Update, Op::Covariance>());
}

template<>
inline void smoother<true, true> (
  FitNode& node
) {
  FitMathNonVec::average (
    node.getState<Op::Forward, Op::Update, Op::StateVector>(),
    node.getState<Op::Forward, Op::Update, Op::Covariance>(),
    node.getState<Op::Backward, Op::Update, Op::StateVector>(),
    node.getState<Op::Backward, Op::Update, Op::Covariance>(),
    node.getSmooth<Op::StateVector>(),
    node.getSmooth<Op::Covariance>()
  );
}

inline void updateResiduals (
  FitNode& node
) {
  PRECISION value {0.0}, error {0.0};
  if (node.m_measurement != nullptr) {
    const TrackVectorContiguous& H  = node.m_projectionMatrix;
    PRECISION HCH;
    FitMathNonVec::similarity_5_1 (
      node.getSmooth<Op::Covariance>(),
      H.fArray,
      &HCH);
    
    const TrackVectorContiguous& refX = node.m_refVector.m_parameters;
    const PRECISION V = node.m_errMeasure * node.m_errMeasure;
    const PRECISION sign = node.m_type == HitOnTrack ? -1 : 1;
    value = node.m_refResidual + (H * (refX - node.getSmooth<Op::StateVector>()));
    error = V + sign * HCH;
  }
  *node.m_smoothState.m_residual = value;
  *node.m_smoothState.m_errResidual = error;
}

template<long unsigned W>
inline uint16_t smoother_vec (
  fp_ptr_64_const s1,
  fp_ptr_64_const s2,
  fp_ptr_64_const c1,
  fp_ptr_64_const c2,
  fp_ptr_64 ss,
  fp_ptr_64 sc
) {
  return FitMathCommon<W>::average (
    s1,
    c1,
    s2,
    c2,
    ss,
    sc
  );
}

template<long unsigned W>
inline void updateResiduals_vec (
  std::array<SchItem, W>& n,
  fp_ptr_64_const ss,
  fp_ptr_64_const sc,
  fp_ptr_64 res,
  fp_ptr_64 errRes
) {
  _aligned const std::array<PRECISION, 5*W> pm = ArrayGen::getCurrentProjectionMatrix(n);
  _aligned const std::array<PRECISION, 5*W> pa = ArrayGen::getCurrentParameters(n);
  _aligned const std::array<PRECISION, W> em = ArrayGen::getErrMeasure2(n);
  _aligned const std::array<PRECISION, W> rr = ArrayGen::getRefResidual(n);

  FitMathCommon<W>::updateResiduals (
    n,
    pm,
    pa,
    em,
    rr,
    ss,
    sc,
    res,
    errRes
  );
}

}
