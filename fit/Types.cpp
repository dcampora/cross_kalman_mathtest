#include "Types.h"

namespace VectorFit {

StateVector::StateVector (const StateVectorAOS& state) {
  m_parameters = state.m_parameters;
  m_z = state.m_z;
}

FitParameters::FitParameters (const FitNodeAOS& node, const int& direction) {
  m_transportMatrix = (direction==Forward ? node.m_transportMatrix : node.m_invertTransportMatrix);
}

Node::Node (const FitNodeAOS& node) {
  m_type = node.m_type;
  m_refVector = node.m_refVector;
  m_errMeasure = node.m_errMeasure;
  m_projectionMatrix = node.m_projectionMatrix;
  m_measurement = node.m_measurement;
}

FitNode::FitNode (const FitNodeAOS& node, const unsigned& index)
  : Node(node), m_forwardFit(node, Forward), m_backwardFit(node, Backward),
  m_noiseMatrix(node.m_noiseMatrix), m_transportVector(node.m_transportVector),
  m_refResidual(node.m_refResidual),
  m_index(index) {}

FitNode::FitNode (const FitNodeAOS& node)
  : Node(node), m_forwardFit(node, Forward), m_backwardFit(node, Backward),
  m_noiseMatrix(node.m_noiseMatrix), m_transportVector(node.m_transportVector),
  m_refResidual(node.m_refResidual) {}

Track::Track (
  const std::vector<FitNodeAOS>& track,
  const unsigned& trackNumber,
  GrowingMemManager& memManager
) : m_index(trackNumber) {
  // Generate the nodes
  int i=0;
  for (const auto& node : track) {
    m_nodes.push_back(FitNode(node, i++));
  }

  // Copy the initial forward predicted and backward predicted covariance
  m_initialForwardCovariance  = track.front().m_predictedState[0].m_covariance;
  m_initialBackwardCovariance = track.back().m_predictedState[1].m_covariance;

  // Copy the initial m_parent_trackParameters
  m_parent_nTrackParameters = track.front().m_parent_nTrackParameters;

  // Copy all other state and covariances (to avoid 0x0 accesses - this shouldn't be in Gaudi)
  for (int i=0; i<m_nodes.size(); ++i) {
    const FitNodeAOS& oldnode = track[i];
    FitNode& node = m_nodes[i];

    node.getFit<Op::Forward>().m_states.setBasePointer(States(memManager.getNextElement()));
    node.getFit<Op::Backward>().m_states.setBasePointer(States(memManager.getNextElement()));

    node.getState<Op::Forward, Op::Update, Op::StateVector>() = oldnode.m_filteredState[0].m_stateVector;
    node.getState<Op::Forward, Op::Update, Op::Covariance>() = oldnode.m_filteredState[0].m_covariance;
    node.getState<Op::Backward, Op::Update, Op::StateVector>() = oldnode.m_filteredState[1].m_stateVector;
    node.getState<Op::Backward, Op::Update, Op::Covariance>() = oldnode.m_filteredState[1].m_covariance;
  }

  m_forwardUpstream = 0;
  m_backwardUpstream = track.size() - 1;

  bool foundForward = false;
  bool foundBackward = false;
  for (int i=0; i<track.size(); ++i) {
    const int reverse_i = track.size() - i - 1;

    if (!foundForward && track[i].m_type == HitOnTrack) {
      foundForward = true;
      m_forwardUpstream = i;
    }

    if (!foundBackward && track[reverse_i].m_type == HitOnTrack) {
      foundBackward = true;
      m_backwardUpstream = i;
    }
  }
}

}
