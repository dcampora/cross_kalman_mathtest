#pragma once

#ifdef SP
#define PRECISION float
#else
#define PRECISION double
#endif

// Oldfit types, just to do the conversion to the new types
#include "../oldfit/MatrixTypes.h"
#include "../oldfit/Types.h"

typedef FitNode FitNodeAOS;
typedef TrackVector TrackVectorAOS;
typedef TrackMatrix TrackMatrixAOS;
typedef StateVector StateVectorAOS;

// Some align definitions
#define _aligned alignas(sizeof(PRECISION)*VECTOR_WIDTH)
#ifdef __INTEL_COMPILER
#define _type_aligned __attribute__((align_value(sizeof(PRECISION)*VECTOR_WIDTH)))
#else
#define _type_aligned __attribute__((aligned(sizeof(PRECISION)*VECTOR_WIDTH)))
#endif

typedef _type_aligned PRECISION * const __restrict__ fp_ptr_64;
typedef _type_aligned const PRECISION * const __restrict__ fp_ptr_64_const;

namespace VectorFit {

struct TestNode {
  // input
  _aligned std::array<PRECISION, 5> Xref;
  _aligned std::array<PRECISION, 5> H;
  _aligned PRECISION refResidual;
  _aligned PRECISION errorMeas2;
  _aligned std::array<PRECISION, 25> tm;
  _aligned std::array<PRECISION, 15> nm;
  _aligned std::array<PRECISION, 5> tv;
};

template<unsigned long W>
struct TestNodeOut {
  _aligned std::array<PRECISION, 5*W> px;
  _aligned std::array<PRECISION, 15*W> pc;
  _aligned std::array<PRECISION, 5*W> x;
  _aligned std::array<PRECISION, 15*W> c;
  _aligned std::array<PRECISION, W> chi2;
};

struct TestNodeSmoother {
  _aligned std::array<PRECISION, 5> pa;
  _aligned std::array<PRECISION, 5> pm;
  _aligned PRECISION refResidual;
  _aligned PRECISION errorMeas2;
};

template<unsigned long W>
struct TestNodeOutSmoother {
  _aligned std::array<PRECISION, 5*W> s1;
  _aligned std::array<PRECISION, 5*W> s2;
  _aligned std::array<PRECISION, 15*W> c1;
  _aligned std::array<PRECISION, 15*W> c2;
  _aligned std::array<PRECISION, 5*W> ss;
  _aligned std::array<PRECISION, 15*W> sc;
  _aligned std::array<PRECISION, W> res;
  _aligned std::array<PRECISION, W> errRes;
};

}
