#pragma once

#include "Types.h"
#include "FitMath.h"
#include "ArrayGen.h"

namespace VectorFit {

template<class D>
inline void update (
  FitNode& node
) {
  if (node.m_type == HitOnTrack) {
    FitMathNonVec::update (
      node.getState<D, Op::Update, Op::StateVector>(),
      node.getState<D, Op::Update, Op::Covariance>(),
      node.getChi2<D>(),
      node.m_refVector.m_parameters.fArray,
      node.m_projectionMatrix.fArray,
      node.m_refResidual,
      node.m_errMeasure * node.m_errMeasure
    );
  }
}

template<unsigned W>
inline void update_vec (
  const std::array<PRECISION, 5*W>& Xref,
  const std::array<PRECISION, 5*W>& H,
  const std::array<PRECISION, W>& refResidual,
  const std::array<PRECISION, W>& errorMeas2,
  fp_ptr_64 us,
  fp_ptr_64 uc,
  fp_ptr_64 chi2
) {
  FitMathCommon<W>::update (
    us,
    uc,
    chi2,
    Xref,
    H,
    refResidual,
    errorMeas2
  );
}

}
